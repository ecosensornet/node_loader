#include "src/board.h"
#include "src/node.h"
#include "src/settings.h"


void setup() {
#if (DEBUG == 1)
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }
  delay(300);
  Serial.print("started in :");
  Serial.print(millis());
  Serial.println(" [ms]");
  delay(10000);
#endif  // DEBUG

  board::setup();
  node::setup();
}

void loop() {
  board::signal_normal();
  node::loop();
}
