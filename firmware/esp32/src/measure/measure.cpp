#include <ArduinoJson.h>

#include "../calibration/calibration.h"

#include "measure.h"

RTC_DATA_ATTR uint16_t cur_ind = 0;
measure::Sample sample_cur;

void measure::setup() {
    sample_cur.ind = cur_ind;
}


void measure::sample(){
    if (cur_ind == 65535) {
        cur_ind = 0;
    } else {
        cur_ind++;
    }
    sample_cur.ind = cur_ind;
}
