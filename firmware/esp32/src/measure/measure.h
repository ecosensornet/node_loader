#pragma once

#include "../settings.h"

namespace measure {

typedef struct {
  uint16_t ind;
} Sample;

void setup();
void sample();

}  // namespace measure

extern measure::Sample sample_cur;
