#include <Arduino.h>

#include "calibration/calibration.h"
#include "config/config.h"
#include "measure/measure.h"
#include "mode_config/mode_config.h"
#include "mode_espnow/mode_espnow.h"

#include "node.h"

static const uint8_t PIN_MODE_CONFIG = 13;


void node::setup() {
  pinMode(PIN_MODE_CONFIG, INPUT_PULLUP);
#if (DEBUG == 1)
  Serial.println("Setup, mode: " + String(cfg_mode));
#endif // DEBUG
  config::setup();
  calibration::setup();

  if (cfg_mode != MODE_CONFIG & digitalRead(PIN_MODE_CONFIG) == LOW) {
#if (DEBUG == 1)
    Serial.println("Force MODE_CONFIG");
#endif // DEBUG
    cfg_mode = MODE_CONFIG;
    ESP.deepSleep(0);
  }

  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::setup();
      break;
    case MODE_ESPNOW:
      mode_espnow::setup();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
      delay(5000);
#endif // DEBUG
      break;
  }

  // measures
  measure::setup();
}


void node::loop() {
  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::loop();
      break;
    case MODE_ESPNOW:
      mode_espnow::loop();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
#endif // DEBUG
      break;
  }
}
