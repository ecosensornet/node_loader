#include <ArduinoJson.h>
#include <LittleFS.h>

#include "calibration.h"

calibration::Calibration calib;

void calibration::setup() {
  calibration::load_file();
}


void calibration::load_file() {
  uint8_t i;
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("/calib.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}


void calibration::save_file() {
  uint8_t i;
  StaticJsonDocument<384> doc;

  File fhw = LittleFS.open("/calib.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved calib:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif

  yield();
}
