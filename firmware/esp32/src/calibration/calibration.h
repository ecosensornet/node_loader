#pragma once

#include "../settings.h"


namespace calibration {

typedef struct {} Calibration;

void setup();
void load_file();
void save_file();

}  // namespace calibration

extern calibration::Calibration calib;
