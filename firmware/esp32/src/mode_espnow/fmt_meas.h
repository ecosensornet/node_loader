#pragma once

#include <Arduino.h>

#include "../settings.h"

namespace mode_espnow {

union {
    uint16_t val;
    uint8_t bytes[2];
} cvtd;

uint8_t fmt_meas(uint8_t* msg);

}  // namespace mode_espnow
