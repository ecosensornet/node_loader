#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


uint8_t mode_espnow::fmt_meas(uint8_t* msg) {
  uint8_t i;

  // interface id
  msg[0] = 0;

  // sensor1
  cvtd.val = sample_cur.ind;
  for (i=0;i<2;i++){
    msg[1+i] = cvtd.bytes[i];
  }

  return 3;
}

