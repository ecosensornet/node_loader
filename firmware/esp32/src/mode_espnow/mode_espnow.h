#pragma once

#include <esp_now.h>

#include "../settings.h"

namespace mode_espnow {

void on_data_sent(const uint8_t*, esp_now_send_status_t);
void setup();
void loop();

}  // namespace mode_espnow
