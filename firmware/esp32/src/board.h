#pragma once

#include "settings.h"

#define LED_SIGNAL 15

namespace board {

void setup();
void signal_normal();

}  // namespace board
