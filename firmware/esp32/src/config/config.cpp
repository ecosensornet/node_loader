#include <ArduinoJson.h>
#include <WiFi.h>
#include <LittleFS.h>

#include "config.h"

RTC_DATA_ATTR uint8_t cfg_mode = MODE_CONFIG;
NodeConfig cfg;
char node_id[13];


void config::setup() {
  LittleFS.begin();
  load_file();
  fetch_node_id(node_id);
}


void config::load_file() {
  StaticJsonDocument<384> doc;
  File fhr = LittleFS.open("/config.json", "r");
  deserializeJson(doc, fhr);
  fhr.close();

#if (DEBUG == 1)
  Serial.println("Loaded config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif // DEBUG

  cfg.interval = doc["measure"]["interval"];
  for (uint8_t i = 0; i < 6; i++) {
    cfg.gate_ip[i] = doc["mode_espnow"]["gateway"][i];
  }

  yield();
}


void config::save_file() {
  StaticJsonDocument<384> doc;

  doc["measure"]["interval"] = cfg.interval;

  JsonArray mode_espnow_gateway = doc["mode_espnow"].createNestedArray("gateway");
  for (uint8_t i = 0; i < 6; i++) {
    mode_espnow_gateway.add(cfg.gate_ip[i]);
  }

  File fhw = LittleFS.open("/config.json", "w");
  serializeJsonPretty(doc, fhw);
  fhw.close();

#if (DEBUG == 1)
  Serial.println("Saved config:");
  String msg = "";
  serializeJsonPretty(doc, msg);
  Serial.println(msg);
#endif // DEBUG

  yield();
}


void config::fetch_node_id(char* buffer) {
  uint8_t maca[6];
  WiFi.macAddress(maca);

  sprintf(buffer,
          "%02x%02x%02x%02x%02x%02x",
          maca[0], maca[1], maca[2], maca[3], maca[4], maca[5]
         );
}
