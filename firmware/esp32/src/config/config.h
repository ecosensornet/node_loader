#pragma once

#include <Arduino.h>

#include "../settings.h"

#define MODE_CONFIG 0
#define MODE_ESPNOW 1

typedef struct {
  uint16_t interval;  // [s] time between two measures

  // local espnow settings
  uint8_t gate_ip[6];  // MAC address of gateway

} NodeConfig;

extern uint8_t cfg_mode;
extern NodeConfig cfg;
extern char node_id[13];

namespace config {

void setup();
void load_file();
void save_file();

void fetch_node_id(char*);

}
