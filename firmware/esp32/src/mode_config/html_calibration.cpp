#include <Arduino.h>

#include "../calibration/calibration.h"
#include "../config/config.h"

#include "html_calibration.h"
#include "mode_config.h"


String html_calibration::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }

  return "Key not found";
}


void html_calibration::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to calibration.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    for (uint8_t i = 0; i < server.args(); i++) {
    }
    calibration::save_file();
  }

  server.process_and_send("/page_calibration.html", html_calibration::processor);
}

