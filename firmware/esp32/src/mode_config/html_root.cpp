#include <Arduino.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"
#include "html_root.h"
#include "mode_config.h"

String meas_cur = "none";

String html_root::processor(const String& key) {
  if (key == "val_cur") {
    return meas_cur;
  }
  else if (key == "nid") {
    return node_id;
  }

  return "Key not found";
}


void html_root::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving root");
#endif
  if (server.method() == HTTP_POST) {
#if (DEBUG == 1)
    Serial.println("POST to root.html");
    String message = "POST form was:\n";
    for (uint8_t i = 0; i < server.args(); i++) {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    Serial.println(message);
#endif

    measure::sample();
    meas_cur = "";
    mode_config::fmt_meas(meas_cur);
  }

  server.process_and_send("/page_root.html", html_root::processor);
}

void html_root::handle_favicon(){
#if (DEBUG == 1)
  Serial.println("serving favicon.ico");
#endif
  server.send_plain("favicon");
}


void html_root::handle_style() {
#if (DEBUG == 1)
  Serial.println("serving style.css");
#endif
  server.send("/style.css", "text/css");
}


void html_root::handle_not_found() {
#if (DEBUG == 1)
  Serial.println("serving page not found: " + server.uri());
#endif
  server.send("/page_not_found.html", "text/html");
}
