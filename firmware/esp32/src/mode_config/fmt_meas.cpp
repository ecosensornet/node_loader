#include <ArduinoJson.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


void mode_config::fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["ind"] = sample_cur.ind;

  serializeJsonPretty(doc, msg);
}

