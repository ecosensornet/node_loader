#pragma once

#include "../settings.h"

namespace html_calibration {

String processor(const String& key);
void handle_html();

}  // namespace html_calibration
