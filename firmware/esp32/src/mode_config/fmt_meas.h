#pragma once

#include "../settings.h"

namespace mode_config {

void fmt_meas(String& msg);

}  // namespace mode_config
